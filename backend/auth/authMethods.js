const jwt = require('jsonwebtoken');
const auth = require('./_auth');
const { getUsuarioBy } = require('../service/UsuarioService');

exports.Bearer = function(req, def, token, cb) {
	if (!token) return cb(new Error('Usuário não autenticado'));

	let parsedToken = auth.parseToken(token);
	jwt.verify(parsedToken, 'bigode', function(err, decoded) {
	    if (err) return cb(new Error('Token de acesso inválido'))
	    else {
	    	req.accessToken = auth.decodeToken(parsedToken);
	    	return cb()
	    }
	});
}