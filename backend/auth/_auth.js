//@ts-check
const jwt = require('jsonwebtoken');
const crypto = require('crypto')

exports.genSalt = function(length) {
  return crypto.randomBytes(Math.ceil(length/2))
    .toString('hex') 
    .slice(0,length);
}

exports.hashPasswordWithSalt = function(rawPassword, salt) {
  let hash = crypto.createHmac('sha512', salt).update(rawPassword);
  let hashedPassword = hash.digest('hex');
  return {
      salt: salt,
      hash: hashedPassword
  }
}

exports.hashPassword = function(rawPassword) {
  let salt = exports.genSalt(5); 
  return exports.hashPasswordWithSalt(rawPassword, salt);
}

exports.genRefreshToken = function(user) {
  return user._id.toString() + crypto.randomBytes(256)
    .toString('hex')
};

exports.genAccessToken = function(user) {
  return jwt.sign({ 
    id: user.id,
    nome: user.nome, 
    email: user.email
  }, 'bigode', { expiresIn: '7d' });
}

exports.parseToken = function(authHeader) {
  const token = authHeader.split(' ');
  return token[token.length - 1];
}

exports.decodeToken = function(token) {
  return jwt.verify(token, 'bigode');
}