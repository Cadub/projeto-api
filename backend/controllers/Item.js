'use strict';

var utils = require('../utils/writer.js');
var Item = require('../service/ItemService');

module.exports.createItem = function createItem (req, res, next) {
  var leilaoId = req.swagger.params['leilaoId'].value;
  Item.createItem(leilaoId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteItem = function deleteItem (req, res, next) {
  var itemId = req.swagger.params['itemId'].value;
  Item.deleteItem(itemId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.listItem = function listItem (req, res, next) {
  Item.listItem()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.listItemsByLeilao = function listItemsByLeilao (req, res, next) {
  var leilaoId = req.swagger.params['leilaoId'].value;
  Item.listItemsByLeilao(leilaoId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.patchItem = function patchItem (req, res, next) {
  var itemId = req.swagger.params['itemId'].value;
  Item.patchItem(itemId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.showItemById = function showItemById (req, res, next) {
  var itemId = req.swagger.params['itemId'].value;
  Item.showItemById(itemId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
