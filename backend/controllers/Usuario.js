//@ts-check
'use strict';

const utils = require('../utils/writer.js');
const Usuario = require('../service/UsuarioService');

module.exports.getPerfil = function (req, res, next) {
	Usuario.getPerfil(req.accessToken)
		.then(function (response) {
			utils.writeJson(res, response);
		})
		.catch(function (response) {
			utils.writeJson(res, response);
		});
}

module.exports.getUsuario = function (req, res, next) {
	const usuarioId = req.swagger.params['usuarioId'].value;

	Usuario.getUsuario(usuarioId)
		.then(function (response) {
			utils.writeJson(res, response);
		})
		.catch(function (response) {
			utils.writeJson(res, response);
		});
}

module.exports.loginUsuario = function (req, res, next) {
	const email = req.swagger.params['loginUsuario'].value.email;
	const senha = req.swagger.params['loginUsuario'].value.senha;

	Usuario.loginUsuario(email, senha)
		.then(function (response) {
			utils.writeJson(res, response);
		})
		.catch(function (response) {
			const Objresponse = { error: response }
			utils.writeJson(res, Objresponse);
		});
}

module.exports.newUsuario = function (req, res, next) {
	const usuario = req.swagger.params['usuario'].value;

	Usuario.newUsuario(usuario)
		.then(function (response) {
			utils.writeJson(res, response);
		})
		.catch(function (response) {
			utils.writeJson(res, response);
		});
};
module.exports.getPerfilLeiloes = function (req, res, next) {
	Usuario.getPerfilLeiloes(req.accessToken)
		.then(function (response) {
			utils.writeJson(res, response);
		})
		.catch(function (response) {
			utils.writeJson(res, response);
		});
}
module.exports.addToken = function (req, res, next) {
	const token = req.swagger.params['token'].value.token;
	Usuario.addToken(req.accessToken, token)
		.then(function (response) {
			utils.writeJson(res, response);
		})
		.catch(function (response) {
			utils.writeJson(res, response);
		});
}

