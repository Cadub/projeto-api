'use strict';

const utils = require('../utils/writer.js');
const Leilao = require('../service/LeilaoService');
const auth = require('../auth/_auth');

module.exports.getLeiloes = function(req, res, next) {
    const status = req.swagger.params['status'].value;

    Leilao.getLeiloes(status)
        .then(response => {
            utils.writeJson(res, response);
        })
        .catch(response => utils.writeJson(res, response))
}

module.exports.newLeilao = function(req, res, next) {
    const leilao = req.swagger.params['leilao'].value;
    
    Leilao.newLeilao(leilao, req.accessToken)
        .then(response => {
            utils.writeJson(res, response);
        })
        .catch(response => utils.writeJson(res, response))
}

module.exports.getLeilao = function(req, res, next) {
    const leilaoId = req.swagger.params['leilaoId'].value;

    Leilao.getLeilao(leilaoId)
        .then(response => {
            utils.writeJson(res, response);
        })
        .catch(response => utils.writeJson(res, response))
}

module.exports.editLeilao = function(req, res, next) {
    const leilaoId = req.swagger.params['leilaoId'].value;
    let userId;

    Leilao.editLeilao(leilaoId, userId)
        .then(response => {
            utils.writeJson(res, response);
        })
        .catch(response => utils.writeJson(res, response))
}

module.exports.deleteLeilao = function(req, res, next) {
    const leilaoId = req.swagger.params['leilaoId'].value;
    let userId;

    Leilao.deleteLeilao(leilaoId, userId)
        .then(response => {
            utils.writeJson(res, response);
        })
        .catch(response => utils.writeJson(res, response))
}

module.exports.getLances = function(req, res, next) {
    const leilaoId = req.swagger.params['leilaoId'].value;

    Leilao.getLances(leilaoId)
        .then(response => {
            utils.writeJson(res, response);
        })
        .catch(response => utils.writeJson(res, response))
}

module.exports.newLance = function(req, res, next) {
    const leilaoId = req.swagger.params['leilaoId'].value;
    const valor = req.swagger.params['valor'].value.valor

    Leilao.newLance(leilaoId, valor, req.accessToken)
        .then(response => {
            utils.writeJson(res, response);
        })
        .catch(response => utils.writeJson(res, response))
}

module.exports.getLance = function(req, res, next) {
    const leilaoId = req.swagger.params['leilaoId'].value;
    const lanceId = req.swagger.params['lanceId'].value;

    Leilao.getLance(leilaoId, lanceId)
        .then(response => {
            utils.writeJson(res, response);
        })
        .catch(response => utils.writeJson(res, response))
}