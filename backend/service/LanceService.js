'use strict';
/**
 * @typedef {{"id": string,"name": string,"email": string,"valor": number,"items": IItem[]}} IUsuario
 */
const mysql = require('./Mysql.service');

// exports.getLance = function(leilaoId, lanceId) {
//   return new Promise(function(resolve, reject) {
//     if (lanceId) resolve(await mysql.query(`SELECT * FROM lance WHERE leilaoId='${mysql.escape(leilaoId)}' AND lanceId=${mysql.escape(lanceId)};`).catch(err => reject(err)))
//     else resolve(await mysql.query(`SELECT * FROM lance WHERE leilaoId='${mysql.escape(leilaoId)}';`);
//   });
// }
/**
 * @param {number} leilaoId
 * @param {IUsuario} valor
 */
exports.newLance = function(leilaoId, valor) {
    let fields = `(usuarioId, valor, data)`;
    let values = `(${userId}, ${valor}, ${new Date})`;

    return new Promise(async(resolve, reject) => {
        resolve(await mysql.query(`INSERT INTO lance ${fields} VALUES ${values};`));
    });
}