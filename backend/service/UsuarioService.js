//@ts-check
'use strict';

const mysql = require('./Mysql.service');
const errors = require("../utils/enviroment")
const fetch = require('node-fetch');
const { genAccessToken, hashPassword, hashPasswordWithSalt } = require('../auth/_auth');
/**
 * @typedef {{"id": string,"name": string,"email": string,"valor": number,"items": import('./LeilaoService').IItem[],"hash":string,"salt":string}} IUsuario
 */

exports.getUsuario = function (usuarioId) {
    const escUsuarioId = mysql.connection.escape(usuarioId);

    return new Promise(async (resolve, reject) => {
        resolve(await mysql.query(`SELECT nome, email FROM Usuario WHERE id=${escUsuarioId};`).catch(err => reject(err)));
    });
}
exports.addToken = function (token, notiToken) {
    return new Promise(async (resolve, reject) => {
        resolve(await mysql.query(`UPDATE Usuario SET token='${notiToken}' WHERE id=${token.id};`).catch(err => reject(err)));
    });
}
exports.getPerfil = function (token) {
    return new Promise(async (resolve, reject) => {
        resolve(await mysql.query(`SELECT nome, email, saldo FROM Usuario WHERE id=${token.id};`).catch(err => reject(err)));
    });
}

exports.newUsuario = function (usuario) {
    const { nome, email, senha } = usuario;
    const { hash, salt } = hashPassword(senha);
    const fields = `(nome, email, hash, salt, saldo)`;
    const values = `("${nome}", "${email}", "${hash}", "${salt}", 0)`;

    return new Promise(async (resolve, reject) => {
        resolve(await mysql.query(`INSERT INTO Usuario ${fields} VALUES ${values};`).catch(err => reject(err)));
    });
}

exports.loginUsuario = function (email, password) {
    return new Promise(async (resolve, reject) => {
        const userData = await mysql.query(`SELECT id, nome, hash, salt FROM Usuario WHERE email="${email}";`).catch(err => reject(err));

        if (!userData.length) { reject(errors.errorMessages.FAIL_TO_LOGIN) } else {
            const creds = hashPasswordWithSalt(password, userData[0].salt);
            if (creds.hash === userData[0].hash) resolve({ token: genAccessToken(userData[0]) })
            else reject(errors.errorMessages.FAIL_TO_LOGIN);
        }
    });
}
exports.getPerfilLeiloes = function (token) {
    return new Promise(async (resolve, reject) => {
        const userData = await mysql.query(`SELECT  l.* ,i.nome as item_nome,i.imagens as item_imagem, i.id as item_id FROM Leilao as l INNER JOIN Item as i on l.id = id_leilao WHERE id_usuario = "${token.id}";`).catch(err => reject(err));
        resolve(userData)

    })
}
exports.NotifyUsers = function (leilaoId) {
    return new Promise(async (resolve, reject) => {
        const userData = await mysql.query(`SELECT u.token From Lance as l INNER JOIN Usuario as u on l.id_usuario = u.id WHERE id_leilao = "${leilaoId}";`).catch(err => reject(err));
        /**
         * @type {RequestInit}
         */
        userData.forEach(i => {
            debugger
            let init = {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization':'KEY=AAAAbzUdm_E:APA91bFAIL6BSB4YzEbHbw2JBFTwBosqTVOLJ6WtCXVKMqro9-P7PIKcnjeRe3xRxjWZ6hwENMNT-T_QLC0saudRi-7-wFVNK8_cv0LkexXqUSmj7nnRNIjh2k5xmUXKqoC_U8phItD2'
                },
                method: "POST",

                body: JSON.stringify(
                    {
                        "notification": {
                            "title": "Smart-auction",
                            "body": "Leilao finalizado"
                        },
                        "to": i.token
                    }
                )
            }

            fetch('https://fcm.googleapis.com/fcm/send', init).then(async i=>{
               const x=await  i.json()
               debugger
            })
        });
    })
}