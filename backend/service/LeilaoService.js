/**
 * @typedef {{"id": number, "id_leilao": number, "nome": string, "imagens": string}} IItem 
 * @typedef {{"id": number, "nome": string, "email": string, "hash": string, "salt": string, "saldo": number}} IUsuario
 * @typedef {{"id": number, "id_leilao": number, "id_usuario": number, "valor": float, "data": string}} ILance
 * @typedef {{"id": number, "id_usuario":number, "ativo": boolean, "nome": string, "valor_inicial": number, "valor_aumento": number, "data_inicial": string, "data_final": string}} ILeilao 
 */

'use strict';

var moment = require('moment-timezone');
const utils = require("../utils/sqlTools")
const killers = require('./killers');
const mysql = require('./Mysql.service');
const errors = require("../utils/enviroment")

exports.LeilaoKiller = () => {
    return new Promise(async(result, reject) => {
        const data = await mysql.query("SELECT now() as now, id as id, data_final as data_final FROM Leilao WHERE Leilao.data_final <= now() + INTERVAL 5 MINUTE AND Leilao.ativo IS TRUE")
            .catch(err => console.log(err))
        data.forEach(element => {
            const now = moment().tz('America/Asuncion')
            const now2 = moment(now.format("MM/DD/YYYY hh:mm"),"MM/DD/YYYY hh:mm")
            const dtFinal = moment(element.data_final)
            let ms = dtFinal.diff(now2);
            killers.addToGreenMile(element.id, ms);
        });
    })
}

exports.getLeiloes = function(status) {
    let query = `SELECT l.* ,i.nome as item_nome,i.imagens as item_imagem, i.id as item_id FROM Leilao as l INNER JOIN Item as i on l.id = id_leilao`;
    if (status) {
        status = status.value === 'concluido' ?
            false :
            true;
        query += ` WHERE ativo=${status};`
    } else query += `;`;

    return new Promise(async(resolve, reject) => {
        const results = resolve(await mysql.query(query).catch(err => reject(err)));
        resolve({ results });
    });
}

/**
 * @param {ILeilao} leilao
 */
exports.newLeilao = async function(leilao, token) {
    console.log(leilao)
    let fields = `(ativo, valor_inicial, valor_aumento, data_inicial, data_final, id_usuario, nome)`;
    let values = `(TRUE, ${leilao.valor_entrada}, 50, "${new Date().toJSON()}", "${new Date(leilao.end_date).toJSON()}", ${token.id}, "${leilao.nome}")`;

    return new Promise(async(resolve, reject) => {
        try {
            const results = await mysql.query(`INSERT INTO Leilao ${fields} VALUES ${values};`).catch(err => reject(err));
            const insertId = results.insertId;
            await mysql.query(`INSERT INTO Item(nome, imagens, id_leilao) VALUES ("${leilao.nome}", "${leilao.item_image}", ${insertId})`);
            resolve({
                'message': 'Sucesso',
                '_links': {
                    'leilao': `http://localhost:8080/leilao/${insertId}`
                }
            });
        } catch (e) {
            reject(e);
        }
    });
}

exports.getLeilao = function(leilaoId) {
    let escLeilaoId = (leilaoId);

    return new Promise(async(resolve, reject) => {
        /**
         * @type {[]}
         */
        const results = await mysql.query(`SELECT l.* ,i.nome as item_nome,i.imagens as item_imagem, i.id as item_id FROM Leilao as l INNER JOIN Item as i on l.id = id_leilao WHERE l.id='${escLeilaoId}' LIMIT 1;`).catch(err => reject(err));
        if (results.length) {
            resolve({
                results: results[0],
                '_links': {
                    'usuario': `http://localhost:8080/usuario/${results[0].id_usuario}`,
                    'lances': `http://localhost:8080/leilao/${results[0].id}/lances`
                }
            });
        } else {
            reject(errors.errorMessages.NO_DATA)
        }

    });
}

// ****
exports.editLeilao = function(leilaoId) {
    let escLeilaoId = mysql.connection.escape(leilaoId);

    return new Promise(async(resolve, reject) => {
        const results = await mysql.query(`UPDATE Leilao SET ativo=FALSE WHERE id=${escLeilaoId}`).catch(err => reject(err))
        resolve({ 'message': 'Sucesso' });
    });
}

// ****
exports.deleteLeilao = function(leilaoId) {
    let escLeilaoId = mysql.connection.escape(leilaoId);

    return new Promise(async(resolve, reject) => {

        const deleteItem = await mysql.query(`DELETE FROM Item WHERE id_leilao=${escLeilaoId}`).catch(err => reject(err));
        const deleteLance = await mysql.query(`DELETE FROM Leilao WHERE id=${escLeilaoId}`).catch(err => reject(err));
        resolve({ 'message': 'Sucesso' });
    });
}

exports.getLances = function(leilaoId) {
    let escLeilaoId = mysql.connection.escape(leilaoId);

    return new Promise(async(resolve, reject) => {
        const results = await mysql.query(`SELECT l.id,l.id_leilao,l.id_usuario,l.valor as lance_valor,l.data, u.nome,u.email FROM Lance as l INNER JOIN Usuario as u on u.id = l.id_usuario WHERE id_leilao=${escLeilaoId} ORDER BY l.valor DESC;`).catch(err => reject(err));
        resolve({ results });
    });
}

exports.newLance = function(leilaoId, valor, token) {
    return new Promise(async(resolve, reject) => {
        const leilaoQuery = mysql.connection.format('SELECT * FROM Leilao WHERE id=?', [leilaoId]);
        const leilao = await mysql.query(leilaoQuery).catch(err => reject(err));
        // Lance após data de fechamento do leilão;
        const now = moment().tz('America/Asuncion')
        const now2 = moment(now.format("MM/DD/YYYY hh:mm"),"MM/DD/YYYY hh:mm")
        const dtFinal = moment(leilao[0].data_final)

        if (now2.toDate().getTime() >= dtFinal.toDate().getTime()) return reject({ 'message': 'Este leilão já foi encerrado' });

        const usuarioQuery = mysql.connection.format('SELECT saldo FROM Usuario WHERE id=?', [token.id]);
        const usuario = await mysql.query(usuarioQuery).catch(err => reject(err));
        // Saldo insuficiente;
        if (usuario[0].saldo < valor) return reject({ 'message': 'Saldo insuficiente' });

        const lanceQuery = mysql.connection.format('SELECT * FROM Lance WHERE id_leilao=? ORDER BY valor DESC LIMIT 1', [leilaoId]);
        const maiorLance = await mysql.query(lanceQuery).catch(err => reject(err));
        // Houver lance maior que o enviado;
        if (maiorLance && maiorLance.length && valor < maiorLance[0].valor) return reject({ 'message': 'Já existe um lance com valor maior ou igual ao do lance enviado' });
        // Valor menor que valor_aumento;
        if (maiorLance && maiorLance.length && valor < maiorLance[0].valor + leilao[0].valor_aumento) return reject({ 'message': 'Valor inválido' });

        mysql.connection.beginTransaction(async err => {
            if (err) return reject(err);

            const insertLanceQuery = mysql.connection.format(
                `INSERT INTO Lance (id_leilao, id_usuario, valor, data) VALUES (?, ?, ?, ?)`, [leilao[0].id, token.id, valor, new Date()]
            );
            await mysql.query(insertLanceQuery).catch(err => mysql.connection.rollback(() => reject(err)));

            const previousLancesQuery = mysql.connection.format(
                `SELECT * FROM Lance WHERE id_usuario=? ORDER BY valor DESC LIMIT 1`, [token.id]
            );
            const previous = await mysql.query(previousLancesQuery).catch(err => mysql.connection.rollback(() => reject(err)));
            const lastLance = previous.length ? previous[0].valor : 0;

            const updateSaldoQuery = mysql.connection.format(
                `UPDATE Usuario SET saldo=? WHERE id=?`, [usuario[0].saldo - (valor - lastLance), token.id]
            );
            await mysql.query(updateSaldoQuery).catch(err => mysql.connection.rollback(() => reject(err)));

            await mysql.connection.commit(err => {
                if (err) mysql.connection.rollback(() => reject(err))
                else return resolve({ 'message': 'Sucesso' });
            });
        });
    });
}

exports.getLance = function(leilaoId, lanceId) {
    let escLeilaoId = mysql.connection.escape(leilaoId);
    let escLanceId = mysql.connection.escape(leilaoId);

    return new Promise(async(resolve, reject) => {
        const results = await mysql.query(`SELECT * FROM Lance WHERE id_leilao=${escLeilaoId} AND id=${escLanceId};`).catch(err => reject(err));
        resolve({ results });
    });
};