'use strict';

const mysql = require('./Mysql.service');

/**
 * Cria um Item
 *
 * leilaoId String id do leilão a qual o lance pertence
 * no response value expected for this operation
 **/
exports.createItem = function(leilaoId) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Deleta um Item
 *
 * itemId String id do Item para remover
 * no response value expected for this operation
 **/
exports.deleteItem = function(itemId) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Lista todos os items
 *
 * returns List
 **/
exports.listItem = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "dono" : {
    "name" : "name",
    "valor" : 6.02745618307040320615897144307382404804229736328125,
    "id" : "id",
    "items" : [ null, null ],
    "email" : "email"
  },
  "name" : "name",
  "id" : "id"
}, {
  "dono" : {
    "name" : "name",
    "valor" : 6.02745618307040320615897144307382404804229736328125,
    "id" : "id",
    "items" : [ null, null ],
    "email" : "email"
  },
  "name" : "name",
  "id" : "id"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Lista todos os items
 *
 * leilaoId String id do leilão a qual o lance pertence
 * returns List
 **/
exports.listItemsByLeilao = function(leilaoId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "dono" : {
    "name" : "name",
    "valor" : 6.02745618307040320615897144307382404804229736328125,
    "id" : "id",
    "items" : [ null, null ],
    "email" : "email"
  },
  "name" : "name",
  "id" : "id"
}, {
  "dono" : {
    "name" : "name",
    "valor" : 6.02745618307040320615897144307382404804229736328125,
    "id" : "id",
    "items" : [ null, null ],
    "email" : "email"
  },
  "name" : "name",
  "id" : "id"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Edita um Item
 *
 * itemId String id do Item para editar
 * no response value expected for this operation
 **/
exports.patchItem = function(itemId) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Retorna um Item especifico
 *
 * itemId String id do Item para retornar
 * returns Item
 **/
exports.showItemById = function(itemId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "dono" : {
    "name" : "name",
    "valor" : 6.02745618307040320615897144307382404804229736328125,
    "id" : "id",
    "items" : [ null, null ],
    "email" : "email"
  },
  "name" : "name",
  "id" : "id"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

