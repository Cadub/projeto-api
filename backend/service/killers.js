const mysql = require('./Mysql.service')

class Killers {
    constructor() {
        /**@type {{id:string,timeOut:string}[]} */
        this.cells = []
    }
    addToGreenMile(id, time) {
        const existis = this.cells.find(i => id === i.id)
        if (!existis) {
            console.log("killing Leilão id:" + id + " in " + (time / 1000) + " seconds")
            if (time < 0) {
                this.killId(id)
            } else {
                this.cells.push({ id, timeOut: setTimeout(() => { this.killId(id) }, time) })
            }
        }
    }
    killId(id) {
        // mysql.query("UPDATE `Leilao` SET `data_conclusao` = now() WHERE `Leilao`.`id` = " + id + ";")
        mysql.connection.beginTransaction(async err => {
            if (err) return reject(err);

            // Finaliza o leilão;
            await mysql.query("UPDATE `Leilao` SET `ativo` = FALSE WHERE `Leilao`.`id` = " + id + ";").catch(err => mysql.connection.rollback(() => reject(err)));;

            // Restaura o saldo dos usuarios que não venceram o leilão;
            await mysql.query(`
                UPDATE Usuario
                INNER JOIN (
                    SELECT id_usuario, MAX(valor) as valor FROM Lance
                    WHERE id_leilao = ${id}
                    AND valor < (SELECT MAX(valor) FROM Lance)
                    AND id_usuario <> (
                        SELECT id_usuario FROM (
                            SELECT id_usuario, valor FROM Lance
                            WHERE valor = (SELECT MAX(valor) FROM Lance)
                        ) valorMax
                    )
                    GROUP BY id_usuario
                ) AS TopLances ON TopLances.id_usuario = Usuario.id
                SET Usuario.saldo = Usuario.saldo + TopLances.valor
            `).catch(err => mysql.connection.rollback(() => reject(err)));;

            // Adiciona o valor do lance vencedor ao saldo do criador do leilão;
            await mysql.query(`
                UPDATE Usuario
                INNER JOIN (
                    SELECT Usuario.id as id_dono_leilao, MAX(Lance.valor) as valor
                    FROM Usuario
                    INNER JOIN Leilao
                        ON Leilao.id = ${id}
                    INNER JOIN Lance
                        ON Lance.id_leilao = ${id}
                    WHERE Usuario.id = Leilao.id_usuario
                ) AS TopLance
                SET Usuario.saldo = Usuario.saldo + TopLance.valor
                WHERE Usuario.id = TopLance.id_dono_leilao
            `).catch(err => mysql.connection.rollback(() => reject(err)));

            await mysql.connection.commit(err => {
                if (err) mysql.connection.rollback(() => reject(err));
                console.log("killed Leitor: " + id);
            });
        });
    }
}

module.exports = new Killers();