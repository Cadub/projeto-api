var mysql = require('mysql');

class MysqlService {
  constructor() {
    this.connection =  mysql.createConnection({
        host: 'remotemysql.com',
        port: 3306,
        user: '9sc2X8uYY8',
        password: 'noiMzPYAPd',
        database: '9sc2X8uYY8'
    });
  }

  connect() {
      return new Promise((resolve,reject)=>{
          this.connection.connect(function (err) {
              if (err) {
                  console.log(`Connection error: ${err.stack}`);
                  reject(err);
              }
              resolve();
          })
      })   
  }
  query(sql) {
      return new Promise((resolve,reject)=>{
          this.connection.query(sql, (err, results, fields) =>{
              if (err) {
                  console.log(`Query error: ${err.stack}`);
                  reject(err)
              }
              resolve(results)
          });
      })
  }

}

module.exports = new MysqlService();
