import { Config } from '@stencil/core';
import { copyFile } from 'fs';

// https://stenciljs.com/docs/config

export const config: Config = {
  globalStyle: 'src/global/app.css',
  globalScript: 'src/global/app.ts',
  copy:[
    {src:"./firebase-messaging-sw.js",dest:"firebase-messaging-sw.js"},
    // {src:"./sw.js",dest:"sw.js"}
  ],
  outputTargets: [
    {
      type: 'www',
      // comment the following line to disable service workers in production
      serviceWorker: null,
      baseUrl: 'https://myapp.local/'
    }
  ]
};
