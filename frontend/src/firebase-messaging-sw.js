importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');
firebase.initializeApp({
    apiKey: "AIzaSyAIfDOXm9k29hRGHfa64i7gGVqjROD4qjs",
    authDomain: "smart-auction-bb163.firebaseapp.com",
    databaseURL: "https://smart-auction-bb163.firebaseio.com",
    projectId: "smart-auction-bb163",
    storageBucket: "smart-auction-bb163.appspot.com",
    messagingSenderId: "477632502769",
    appId: "1:477632502769:web:61ee8880096c6751190250",
    measurementId: "G-351QQD4FZ5"
  });
const messaging = firebase.messaging();