import { GenericService } from "./GenericService";
import { IItem } from "./ambiente";

export class ItemService extends GenericService<IItem>{
    basePath="item/"
    get(id:number): Promise<IItem> {
        return new Promise(async (resolve, reject) => {
            try {
                const value = await this.request("leilao/"+id+"/item/", "GET")
                resolve(value)
            } catch{
                reject()
                console.error('fail to getAll')
            }
        })
    }
    createByLeilao(idLeilao:number,object: IItem):Promise<unknown> { 
        return new Promise(async resolve => {
            try{
            const value = await this.request("leilao/"+idLeilao+"/item","POST",object)
            resolve(value)
            }catch{
                console.error('fail to Create')
            }
        })
    }

}