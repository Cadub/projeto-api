import { GenericService } from "./GenericService";
import { ILeilao, IItem, ILance, ANewLeilao } from "./ambiente";

export class LeilaoService extends GenericService<ILeilao>{
    basePath = "leilao/"
    createWithItem(leilao:ANewLeilao){
        return new Promise(async (resolve, reject) => {
            try {
                const value = await this.request("leilao/", "POST", leilao)
                resolve(value)
            } catch{
                reject()
                console.error('fail to createWithItem')
            }
        })
    }
    getItem(id:number):Promise<IItem>{
        return new Promise(async (resolve, reject) => {
            try {
                const value = await this.request("leilao/"+id+"item/", "GET")
                resolve(value)
            } catch{
                reject()
                console.error('fail to getAll')
            }
        })
    }
    createItem(idLeilao:number,object:IItem){
        return new Promise(async resolve => {
            try{
            const value = await this.request("leilao/"+idLeilao+"/item","POST",object)
            resolve(value)
            }catch{
                console.error('fail to Create')
            }
        })
    }
    getLance(id:number):Promise<ILance[]>{
        return new Promise(async (resolve, reject) => {
            try {
                const value = await this.request("leilao/"+id+"/lances/", "GET")
                resolve(value)
            } catch{
                reject()
                console.error('fail to getAll')
            }
        })
    }
    createLance(idLeilao:number,object:IItem){
        return new Promise(async resolve => {
            try{
            const value = await this.request("leilao/"+idLeilao+"/item","POST",object)
            resolve(value)
            }catch{
                console.error('fail to Create')
            }
        })
    }
}