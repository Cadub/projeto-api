export const API_URL = "https://smart-auction.herokuapp.com/"
export interface IItem {
    "id": string,
    "name": string,
}
export interface IUsuario {
    id:number;
    nome: string;
    email: string;
    saldo: number;
}
export interface ILance {
    id: number;
    id_leilao: number;
    id_usuario: number;
    lance_valor: number;
    data: string;
    nome: string;
    email: string;
}
export interface APIReturn<T> {
    results: T[];
    _links?: Links|any;
}
export interface Links {
    usuario: string;
    lances: string;
}
export interface ILeilao {
    id: number;
    id_usuario: number;
    ativo: number;
    nome: string;
    valor_inicial: number;
    valor_aumento: number;
    data_inicial: Date;
    data_final: Date;
    item_nome: string;
    item_imagem: ItemImagem;
    item_id: number;
}
export interface ItemImagem {
    type: string;
    data: number[];
}
export interface INotification{

}
export interface ILogin {
    id: any;
    token: string
}
export interface ANewLeilao {
    nome: string;
    valor_entrada: number;
    valor_aumento: number;
    end_date: string;
    id_usuario: number;
    item_name: string;
    item_value: string;
    item_image: string;
}