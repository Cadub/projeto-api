import { API_URL } from "./ambiente";
import { LoginStorage } from "./app";

export class GenericService<T>{
    constructor(public basePath: string) { }
    get(id: number): Promise<T> {
        return new Promise(async resolve => {
            try {
                const value = await this.request(this.basePath + id, "GET")
                resolve(value)
            } catch{
                console.error('fail to get')
            }
        })
    }
    create(object: T) {
        return new Promise(async resolve => {
            try {
                const value = await this.request(this.basePath, "POST", object)
                resolve(value)
            } catch{
                console.error('fail to Create')
            }
        })
    }
    delete(id: number) {
        return new Promise(async resolve => {
            try {
                const value = await this.request(this.basePath + id, "DELETE")
                resolve(value)
            } catch{
                console.error('fail to Delete')
            }
        })
    }
    edit(id: number, object: T) {
        return new Promise(async resolve => {
            try {
                const value = await this.request(this.basePath + id, "PATCH", object)
                resolve(value)
            } catch{
                console.error('fail to Edit')
            }
        })
    }
    getAll(): Promise<T[]> {
        return new Promise(async resolve => {
            try {
                const value = await this.request(this.basePath, "GET")
                resolve(value)
            } catch{
                console.error('fail to getAll')
            }
        })
    }
    async request(path: string, method: RequestInit['method'], act?: any): Promise<any> {

        let init: RequestInit = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method,

            body: JSON.stringify(act)
        }

        if (LoginStorage.value && LoginStorage.value.token) {
            init.headers['Authorization'] = LoginStorage.value.token
        }
        if (init.method === "GET") {
            delete init['body']
        }
        try {
            return new Promise((resolve, reject) => {
                fetch(API_URL + path, init).then(async i => {
                    if (i.status != 200 && i.status != 201) {
                        reject()
                    }
                    const value = await i.json()
                    if (value.results) {
                        resolve(value.results)
                    }
                    else {
                        resolve(value);
                    }
                }, async r => { reject(await r); throw ""; })
            })

        }
        catch (e) {
            return await new Promise((resolve) => {
                resolve(new Response(null, { status: 503 }))
            })
        }
    }
}