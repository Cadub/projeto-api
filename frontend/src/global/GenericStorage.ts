import {BehaviorSubject} from 'rxjs'
export class GenericStorege<T>{
    private didUpdate:BehaviorSubject<T> = new BehaviorSubject(null)
    _value:T = null
    get value (){
        return this.update()
    }
    set value(values:T|null){
        this.setAll(values)
    }
    constructor(public path:string){}
    private getAll(){
        const raw = localStorage.getItem(this.path)
        if(raw){
            return JSON.parse(raw)
        }else{
            return ""
        }
    }
    setAll(value:T){
        localStorage.setItem(this.path,JSON.stringify(value))
        this.didUpdate.next(value)
        this.update()
    }
    subscribe(){
        return this.didUpdate.asObservable()
    }
    update(){
        return this.getAll()
    }

}