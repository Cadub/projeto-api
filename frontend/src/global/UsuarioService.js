import { GenericService } from "./GenericService";
export class UsuarioService extends GenericService {
    constructor() {
        super(...arguments);
        this.basePath = "usuario/";
    }
}
