import { GenericService } from "./GenericService";
import { ILance } from "./ambiente";

export class LanceService extends GenericService<ILance>{
    basePath="lance/"
    get(id:number): Promise<ILance> {
        return new Promise(async (resolve, reject) => {
            try {
                const value = await this.request("leilao/"+id+"/item/", "GET")
                resolve(value)
            } catch{
                reject()
                console.error('fail to getAll')
            }
        })
    }
    createByLeilao(idLeilao:number,object: {valor:number}):Promise<any> { 
        return new Promise(async resolve => {
            try{
            const value = await this.request("leilao/"+idLeilao+"/lances","POST",object)
            resolve(value)
            }catch{
                console.error('fail to Create')
            }
        })
    }

}