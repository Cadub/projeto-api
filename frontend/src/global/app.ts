import { ItemService } from "./ItemService"
import { LeilaoService } from "./LeilaoService"
import { LanceService } from "./LanceService"
import { UsuarioService } from "./UsuarioService"
import { GenericStorege } from "./GenericStorage"
import { ILogin } from "./ambiente"
import { NotificationService } from "./NottificationService"

export var itemService = new ItemService("item/")
export var lanceSercive = new LanceService("lance/")
export var usuarioService = new UsuarioService("usuario/")
export var leilaoService = new LeilaoService("leilao/")
export var notificationService = new NotificationService("notification/")
export var LoginStorage = new GenericStorege<ILogin>("login")

export default async () => {
  /**
   * The code to be executed should be placed within a default function that is
   * exported by the global script. Ensure all of the code in the global script
   * is wrapped in the function() that is exported.
   */
};
