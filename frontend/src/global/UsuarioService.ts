import { GenericService } from "./GenericService";
import { IUsuario, ILeilao } from "./ambiente";

export class UsuarioService extends GenericService<IUsuario>{
    basePath = "usuario/"
    login(email:string,senha:string) {
        return new Promise(async resolve => {
            try{
            const value = await this.request(this.basePath+"login","POST",{email,senha})
            resolve(value)
            }catch(e){
                console.error(e)
                console.error('fail to Login')
            }
        })
     }
    getLeiloes():Promise<ILeilao[]>{
        return new Promise(async resolve => {
            try{
            const value = await this.request(this.basePath+"getleiloes","GET")
                resolve(value)
            }catch{
                console.error('fail to GetLeiloes')
            }
        })
    }
    getPerfil():Promise<IUsuario>{
        return new Promise(async resolve => {
            try{
            const value = await this.request(this.basePath+"perfil","GET")
                resolve(value[0])
            }catch{
                console.error('fail to getPerfil')
            }
        })
    }
}