import { API_URL } from "./ambiente";
export class GenericService {
    constructor(basePath) {
        this.basePath = basePath;
    }
    get(id) {
        return new Promise(async(resolve) => {
            try {
                const value = await this.request(this.basePath + id, "GET");
                resolve(value);
            } catch (_a) {
                console.error('fail to get');
            }
        });
    }
    create(object) {
        return new Promise(async(resolve) => {
            try {
                const value = await this.request(this.basePath, "POST", object);
                resolve(value);
            } catch (_a) {
                console.error('fail to Create');
            }
        });
    }
    delete(id) {
        return new Promise(async(resolve) => {
            try {
                const value = await this.request(this.basePath + id, "DELETE");
                resolve(value);
            } catch (_a) {
                console.error('fail to Delete');
            }
        });
    }
    edit(id, object) {
        return new Promise(async(resolve) => {
            try {
                const value = await this.request(this.basePath + id, "PATCH", object);
                resolve(value);
            } catch (_a) {
                console.error('fail to Edit');
            }
        });
    }
    getAll() {
        return new Promise(async(resolve) => {
            try {
                const value = await this.request(this.basePath, "GET");
                resolve(value);
            } catch (_a) {
                console.error('fail to getAll');
            }
        });
    }
    async request(path, method, act) {
        let init = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method,
            body: JSON.stringify(act)
        };
        if (init.method === "GET") {
            delete init['body'];
        }
        try {
            return new Promise((resolve, reject) => {
                fetch(API_URL + path, init).then(async(i) => {
                    if (i.status != 200 && i.status != 201) {
                        reject();
                    }
                    const value = await i.json();
                    if(value.results){
                        resolve(value.results)
                    }
                    else{
                        resolve(value);
                    }
                }, async(r) => { reject(r); throw ""; });
            });
        } catch (e) {
            return await new Promise((resolve) => {
                resolve(new Response(null, { status: 503 }));
            });
        }
    }
}