import { GenericService } from "./GenericService";
import { ILance, INotification } from "./ambiente";

export class NotificationService extends GenericService<INotification>{
    basePath="notification/"
    addToken(token:string): Promise<ILance> {
        return new Promise(async (resolve, reject) => {
            try {
                const obj = {token}
                const value = await this.request(this.basePath, "POST",obj)
                resolve(value)
            } catch{
                reject()
                console.error('fail to AddToekn')
            }
        })
    }
}