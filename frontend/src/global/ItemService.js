import { GenericService } from "./GenericService";
export class ItemService extends GenericService {
    constructor() {
        super(...arguments);
        this.basePath = "item/";
    }
    get(id) {
        return new Promise(async (resolve, reject) => {
            try {
                const value = await this.request("leilao/" + id + "/item/", "GET");
                resolve(value);
            }
            catch (_a) {
                reject();
                console.error('fail to getAll');
            }
        });
    }
    createByLeilao(idLeilao, object) {
        return new Promise(async (resolve) => {
            try {
                const value = await this.request("leilao/" + idLeilao + "/item", "POST", object);
                resolve(value);
            }
            catch (_a) {
                console.error('fail to Create');
            }
        });
    }
}
