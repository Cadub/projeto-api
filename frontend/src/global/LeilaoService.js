import { GenericService } from "./GenericService";
export class LeilaoService extends GenericService {
    constructor() {
        super(...arguments);
        this.basePath = "leilao/";
    }
    getItem(id) {
        return new Promise(async (resolve, reject) => {
            try {
                const value = await this.request("leilao/" + id + "item/", "GET");
                resolve(value);
            }
            catch (_a) {
                reject();
                console.error('fail to getAll');
            }
        });
    }
    createItem(idLeilao, object) {
        return new Promise(async (resolve) => {
            try {
                const value = await this.request("leilao/" + idLeilao + "/item", "POST", object);
                resolve(value);
            }
            catch (_a) {
                console.error('fail to Create');
            }
        });
    }
    getLance(id) {
        return new Promise(async (resolve, reject) => {
            try {
                const value = await this.request("leilao/" + id + "/lance/", "GET");
                resolve(value);
            }
            catch (_a) {
                reject();
                console.error('fail to getAll');
            }
        });
    }
    createLance(idLeilao, object) {
        return new Promise(async (resolve) => {
            try {
                const value = await this.request("leilao/" + idLeilao + "/item", "POST", object);
                resolve(value);
            }
            catch (_a) {
                console.error('fail to Create');
            }
        });
    }
}
