export class GenericStorege<T>{
    _values = []
    get values (){
        return this.update()
    }
    set values(values:T[]){
        this.setAll(values)
    }
    constructor(public path:string){}
    getAll(){
        const raw = localStorage.getItem(this.path)
        if(raw){
            return JSON.parse(raw)
        }else{
            return []
        }
    }
    setAll(value:T[]){
        localStorage.setItem(this.path,JSON.stringify(value))
        this.update()
    }
    update(){
        return this.getAll()
    }

}