import { notificationService } from "./app";

declare let firebase
const firebaseConfig = {
  apiKey: "AIzaSyAIfDOXm9k29hRGHfa64i7gGVqjROD4qjs",
  authDomain: "smart-auction-bb163.firebaseapp.com",
  databaseURL: "https://smart-auction-bb163.firebaseio.com",
  projectId: "smart-auction-bb163",
  storageBucket: "smart-auction-bb163.appspot.com",
  messagingSenderId: "477632502769",
  appId: "1:477632502769:web:61ee8880096c6751190250",
  measurementId: "G-351QQD4FZ5",
  vapidKey: "BE8DE3UnLAACjl-FDg83FZjzEADLiYHShoaVidFv3U8Xh03r7C5Zg_vcycwtkhbp52tWV3uhD1AGMwwDPsqKztE"
};
export const inicializarFirebase = () => {
  firebase.initializeApp(firebaseConfig);
  // navigator.serviceWorker.register('/firebase-messaging-sw.js')
  // .then((registration) => {
  //   firebase.messaging().useServiceWorker(registration);
  // });
}
export const pedirPermissaoParaReceberNotificacoes = async () => {
  try {
    const messaging = firebase.messaging();
    await messaging.requestPermission();
    const token = await messaging.getToken();
    notificationService.addToken(token)
    console.log('token do usuário:', token);
    messaging.usePublicVapidKey(
      firebaseConfig.vapidKey
    );
   

    messaging.onMessage((payload) => {
      console.log(payload);
    });


    return token;
  } catch (error) {
    console.error(error);
  }
}