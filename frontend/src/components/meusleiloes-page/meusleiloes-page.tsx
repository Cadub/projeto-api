import { Component, h, Prop } from '@stencil/core';
import { ILeilao } from '../../global/ambiente';
import { usuarioService } from '../../global/app';

@Component({
  tag: 'meusleiloes-page',
  styleUrl: 'meusleiloes-page.css',
  shadow: false
})
export class MeusleiloesPage {
  
  @Prop({mutable:true}) leiloes:ILeilao[] = []
  @Prop({ mutable: true }) search: string = null
  async componentDidLoad() {
    this.leiloes = await usuarioService.getLeiloes()
    const searchBarInput: HTMLInputElement = document.querySelector(".navSerachBar")
    searchBarInput.oninput = ()=>{
      this.search = searchBarInput.value
    }
  }
  listAll() {
    return this.leiloes.map(i => {
      return <div><Leilao-card-component dono={true} leilao={i}></Leilao-card-component><br></br></div>
    })
  }
  listBySearch() {
    return this.leiloes.filter(i =>{ 
      i.nome = i.nome?i.nome:"Leilão"
      return i.nome.search(this.search) !== -1}).map(i => {
      return <div><Leilao-card-component dono={true} leilao={i}></Leilao-card-component><br></br></div>
    })
  }
  render() {
    return (
      <div class='app-home'>
        {this.search ? this.listBySearch() : this.listAll()}
      </div>
    );
  }

}
