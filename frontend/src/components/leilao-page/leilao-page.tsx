import { Component, h, Prop } from '@stencil/core';
import { MatchResults } from '@stencil/router';
import { leilaoService, lanceSercive } from '../../global/app';
import { ILeilao, ILance } from '../../global/ambiente';

@Component({
  tag: 'app-leilao',
  styleUrl: 'leilao-page.css',
  shadow: false
})
export class LeilaoPage {
  @Prop() match: MatchResults;
  @Prop({ mutable: true }) leilao: ILeilao
  @Prop({ mutable: true }) lances: ILance[]
  @Prop({ mutable: true }) higgerLanceValue = 0
  newLanceValid: boolean;
  async componentWillLoad() {
    this.leilao = await leilaoService.get(+this.match.params.id)
    this.lances = await leilaoService.getLance(this.leilao.id)
    if (this.lances.length) {
      this.higgerLanceValue = this.lances.reduce((i, k) => i.lance_valor > k.lance_valor ? i : k).lance_valor
    }
    else {
      this.higgerLanceValue = this.leilao.valor_inicial
    }
  }
  componentDidLoad() {
    document.getElementById("lanceValue").oninput = () => {
      const form: HTMLFormElement = document.querySelector("#exampleModal > div > div > div.modal-body > form")
      this.newLanceValid = form.checkValidity()
      console.log(this.newLanceValid)
    }
  }
  render() {
    return (
      <div>
        <div>
          <h1 class="leilao name">{this.leilao.nome ? this.leilao.nome : "Leilão"}</h1>
        </div>
        <ul class="list-group">
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Lances
            {this.leilao.ativo?
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Novo Lance</button>:""
            }
          </li>
          {this.lances&& this.lances.length ?
            this.lances.reverse().map(i =>
              <li class="list-group-item">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title">{i.nome}</h5>
                  </div>
                  <div class="lance">
                    <div class="card-body text-center">
                      <p class="card-text">R${i.lance_valor}</p></div>
                  </div>
                  <div class="card-footer text-muted">
                    {new Date(i.data).toLocaleString()}
                  </div>
                </div>
              </li>) :
            <li class="list-group-item disabled">Nenhum lance</li>
          }
        </ul>
        {/*------------modal-----------------*/}
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="form-group">
                    <div class="form-group">
                      <label htmlFor="lanceValue">Qual o valor do lance?</label>
                      <input type="number" class="form-control" min={(this.higgerLanceValue + this.leilao.valor_aumento)} value={(this.higgerLanceValue + this.leilao.valor_aumento)} id="lanceValue" placeholder={"" + (this.higgerLanceValue + this.leilao.valor_aumento)} />
                      <small id="dangerValor" class="form-text text-danger"></small>
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" onClick={() => { this.createLance() }} class="btn btn-primary">Criar</button>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
  async createLance() {
    const valor:HTMLInputElement = document.querySelector("#lanceValue")
    
    const result:{message:string} = await lanceSercive.createByLeilao(this.leilao.id,{valor:+valor.value})
    if(result.message != "Sucesso"){
      const span = document.querySelector('#dangerValor')
      span.textContent = result.message
    }
    else{
      location.reload()
    }

    // location.reload()
  }
}
