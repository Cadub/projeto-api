import { Component, h, Prop } from '@stencil/core';
import { leilaoService } from '../../global/app';
import { ILeilao } from '../../global/ambiente';

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css',
  shadow: false
})
export class AppHome {
  @Prop({ mutable: true }) leiloes: ILeilao[] = [];
  @Prop({ mutable: true }) search: string = null
  async componentDidLoad() {
    const searchBarInput: HTMLInputElement = document.querySelector(".navSerachBar")
    searchBarInput.oninput = () => {
      this.search = searchBarInput.value
    }
    this.leiloes = await leilaoService.getAll()
  }
  listAll(filters: { ativos: boolean }) {
    return this.leiloes.filter(i=>
      !!i.ativo == filters.ativos
    ).map(i => {
      return <div><Leilao-card-component leilao={i}></Leilao-card-component><br></br></div>
    })
  }
  listBySearch(filters: { ativos: boolean }) {
    return this.leiloes.filter(i => {
      i.nome = i.nome ? i.nome : "Leilão"
      if(Boolean(i.ativo) == filters.ativos){ return false}
      return i.nome.search(this.search) !== -1
    }).map(i => {
      return <div><Leilao-card-component leilao={i}></Leilao-card-component><br></br></div>
    })
  }
  render() {
    return (
      <div class='app-home'>
        <a class="d-block p-2 bg-primary text-white text-center" data-toggle="collapse" href="#collapseEnAndamento" role="button" aria-expanded="false" aria-controls="collapseEnAndamento">Em Andamento</a>
        <div class="collapse" id="collapseEnAndamento">
          <div class="showcase">
            {this.search ? this.listBySearch({ ativos: true }) : this.listAll({ ativos: true })}
          </div>
        </div>
        <a class="d-block p-2 bg-dark text-white text-center" data-toggle="collapse" href="#collapsefinalizados" role="button" aria-expanded="false" aria-controls="collapsefinalizados">Finalizados</a>
        <div class="collapse" id="collapsefinalizados">
          <div class="showcase">
            {this.search ? this.listBySearch({ ativos: false }) : this.listAll({ ativos: false })}
          </div>
        </div>
      </div>
    );
  }
}
