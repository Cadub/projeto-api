import { Component, h, Prop } from '@stencil/core';
import { usuarioService, LoginStorage } from '../../global/app';

@Component({
  tag: 'app-login',
  styleUrl: 'app-login.css',
  shadow: false
})
export class AppLogin {
  @Prop({mutable:true}) update = 1
  @Prop({mutable:true}) alertMsg = ""
  email = ""
  senha = ""
  async login(){
    const data:any = await usuarioService.login(this.email,this.senha)
    if(data['error']){
      console.error(data);
      
      this.alertMsg = data.error
      this.update++
      setTimeout(()=>{this.alertMsg = ""},4000)
    }
    else{
      LoginStorage.value = data
    }
  }
  async componentDidLoad(){
    const emailComponent:HTMLInputElement = document.querySelector("#email")
    const senhaComponent:HTMLInputElement = document.querySelector("#senha")
    emailComponent.oninput = (e:any)=>{this.email = e.target.value}
    senhaComponent.oninput = (e:any)=>{this.senha = e.target.value}
    console.log("load")
  }
  render() {
    return (
      <div class='app-login'>
        <div class="loginCard">
          <div class="alert">
            {this.alertMsg}
          </div>
          <input class="loginInput" type="text" id="email" placeholder="Email" />
          <br></br>
          <input class="loginInput" type="password" id="senha" placeholder="Senha" />
          <button class="button loginSubmit" onClick={()=>{this.login()}}>Login</button>
          <a class="cadastroLink" href="/cadastro">Criar conta</a>
        </div>
      </div>
    );
  }
}