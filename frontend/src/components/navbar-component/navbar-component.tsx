import { Component, h, Prop } from '@stencil/core';
import { LoginStorage } from '../../global/app';

@Component({
  tag: 'navbar-component',
  styleUrl: 'navbar-component.css',
  shadow: false
})
export class NavbarComponent {
  @Prop() logged: boolean
  render() {
    return [
      <div class="navZ">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="#">Smart-Auction</a>
          <button style={!this.logged ? { "display": "none" } : null} class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div style={!this.logged ? { "display": "none" } : null} class="collapse navbar-collapse" id="conteudoNavbarSuportado">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
              <stencil-route-link url='/'>
                <a class="nav-link">Home <span class="sr-only">(página atual)</span></a>
                </stencil-route-link>
              </li>
              <li class="nav-item">
              <stencil-route-link url='/profile'>
                <a class="nav-link" >Perfil</a>
                </stencil-route-link>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Leilão
            </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <stencil-route-link url='/newleilao'>
                    <a class="dropdown-item" >Criar novo</a>
                  </stencil-route-link>
                  <stencil-route-link url='/meusleiloes'>
                    <a class="dropdown-item" >Meus Leiloes</a>
                  </stencil-route-link>
                </div>
              </li>
              <li class="nav-item">
                {/* <a class="nav-link disabled" href="#">Desativado</a> */}
              </li>
            </ul>
            <div class="form-inline my-2 my-lg-0">
              <input class="form-control mr-sm-2 navSerachBar" type="search" placeholder="Pesquisar" aria-label="Pesquisar"></input>
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Pesquisar</button>


            </div>
            <div></div>
            <button onClick={() => { LoginStorage.value = null }} class="btn btn-warning">Sair</button>
          </div>
        </nav>
      </div>
    ];
  }
}
