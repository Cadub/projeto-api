import { Component, Prop, h } from '@stencil/core';
import { MatchResults } from '@stencil/router';
import { IUsuario } from '../../global/ambiente';
import { usuarioService } from '../../global/app';

@Component({
  tag: 'app-profile',
  styleUrl: 'app-profile.css',
  shadow: false
})
export class AppProfile {
  @Prop() match: MatchResults;
  @Prop({ mutable: true }) usuario: IUsuario
  async componentDidLoad() {
    this.usuario = await usuarioService.getPerfil()
  }
  render() {

    return (
      <div class="app-profile">
        <div class="profile-header">
          <h1>Perfil Smart-Auction</h1>
        </div>
        <form>
          <div class="row">
            <div class="col">
              <div class="form-group row col-md-12">
                <label class="col-sm-2 col-form-label" htmlFor="name">Nome</label>
                <div class="col-sm-8">
                  <input readOnly class="form-control form-control-lg" id="name" value={this.usuario.nome}></input>
                </div>
              </div>
              <div class="form-group row col-md-12">
                <label class="col-sm-2 col-form-label" htmlFor="email">Email</label>
                <div class="col-sm-8">
                  <input readOnly class="form-control form-control-lg" id="email" value={this.usuario.email}></input>
                </div>
              </div>
            </div>
            <div class="form-group col-md-2">
              <label htmlfor="valor" class="text-center">Saldo em conta</label>
              <input type="number" class="form-control" id="valor" readOnly value={this.usuario.saldo} />
            </div>
          </div>
        </form>
      </div>
    );

  }
}
