var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component, Prop, h } from '@stencil/core';
let AppProfile = class AppProfile {
    normalize(name) {
        if (name) {
            return name.substr(0, 1).toUpperCase() + name.substr(1).toLowerCase();
        }
        return '';
    }
    render() {
        if (this.match && this.match.params.name) {
            return (h("div", { class: "app-profile" },
                h("p", null,
                    "Hello! My name is ",
                    this.normalize(this.match.params.name),
                    ". My name was passed in through a route param!")));
        }
    }
};
__decorate([
    Prop()
], AppProfile.prototype, "match", void 0);
AppProfile = __decorate([
    Component({
        tag: 'app-profile',
        styleUrl: 'app-profile.css',
        shadow: true
    })
], AppProfile);
export { AppProfile };
