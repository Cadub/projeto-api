import { Component, h, Prop } from '@stencil/core';
import { LoginStorage } from '../../global/app';
import { inicializarFirebase, pedirPermissaoParaReceberNotificacoes } from '../../global/push-notification';


@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css',
  shadow: false
})
export class AppRoot {
  @Prop({mutable:true}) logged: boolean;
  constructor(){
    LoginStorage.subscribe().subscribe(()=>{
      this.checkLogged()
    })
    this.checkLogged()
  }
  componentDidLoad(){
    inicializarFirebase()
    pedirPermissaoParaReceberNotificacoes()
  }
  checkLogged(){
      this.logged = !!LoginStorage.value
      console.log(this.logged)
  }
  render() {
    return (
      <div>
        <header>
          
          <navbar-component logged={this.logged}></navbar-component>
        </header>

        <main>
          <stencil-router>
            <stencil-route-switch scrollTopOffset={0}>
              {!this.logged?
              <stencil-route url='/' component='app-login' exact={true} /> :
              <div>
              <stencil-route url='/meusleiloes' component='meusleiloes-page' />                
              <stencil-route url='/' component='app-home' exact={true} />
              <stencil-route url='/leilao/:id' component='app-leilao' />
              <stencil-route url='/profile' component='app-profile' />
              <stencil-route url='/newleilao/:id?' component='newleilao-page' />
              <stencil-route url='/login' component='app-login' />
              </div>
              }
            </stencil-route-switch>
          </stencil-router>
        </main>
      </div>
    );
  }
}
