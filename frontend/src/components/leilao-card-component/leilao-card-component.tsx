import { Component, h, Prop } from '@stencil/core';
import { ILeilao } from '../../global/ambiente';
import { leilaoService} from '../../global/app';
import { RouterHistory } from '@stencil/router';

@Component({
  tag: 'leilao-card-component',
  styleUrl: 'leilao-card-component.css',
  shadow: false
})
export class LeilaoCardComponent {
  @Prop() dono: boolean
  @Prop() leilao: ILeilao
  @Prop() history: RouterHistory;
  render() {
    return (
      <div class="card text-muted">
        <div class="card-header">
          Leilão
          <div class="pills">
            {this.drawToobar()}
          </div>
        </div>
        <div class="card-body">
          <h5 class="card-title">{this.leilao.nome ? this.leilao.nome : "Leilão"}</h5>
          <h6 class="card-subtitle mb-2 text-muted">Item:{this.leilao.item_nome}</h6>
          <a href={"/leilao/" + this.leilao.id} class="card-link">Visualizar</a>
          {/* <a href="#" class="card-link">novo Lance(+R${!this.leilao. ? "50" : this.leilao.lances[this.leilao.lances.length - 1].valor / 2})</a> */}
        </div>
      </div>
    );
  }
  drawToobar() {
    if (this.dono) {
      return (<ul class="nav nav-pills card-header-pills ">
        <li class="nav-item">
          <button class="btn btn-danger" onClick={()=>this.deleteLeilao()}><i class="fas fa-trash"></i></button>
        </li>
        <li class="nav-item">
          <a class="btn btn-secondary" href={"/newleilao/"+this.leilao.id} ><i class="far fa-edit"></i></a>
        </li>
      </ul>
      )
    }
    else {
      return ""
    }
  }
  async deleteLeilao(){
    const conf = confirm("deseja mesmo apagar este leilão?")
    if(!conf) return
    // await itemService.delete(this.leilao.item_id)
    await leilaoService.delete(+this.leilao.id)
    location.reload()
  }
}
