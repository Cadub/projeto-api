import { Component, h, Prop } from '@stencil/core';
import { leilaoService, LoginStorage } from '../../global/app';
import { ANewLeilao, ILeilao } from '../../global/ambiente';
import { MatchResults } from '@stencil/router';

@Component({
  tag: 'newleilao-page',
  styleUrl: 'newlielao-page.css',
  shadow: false
})
export class NewlielaoPage implements ANewLeilao {
  @Prop() match: MatchResults;
  @Prop({mutable:true}) nome
  @Prop({mutable:true}) valor_entrada
  @Prop({mutable:true}) end_date
  @Prop({mutable:true}) id_usuario
  @Prop({mutable:true}) item_name
  @Prop({mutable:true}) item_value
  @Prop({mutable:true}) item_image
  @Prop({mutable:true}) valor_aumento: number;
  leilao: ILeilao;
  async componentDidLoad() {
    
    if (this.match.params.id) {
      this.leilao  = await leilaoService.get(+this.match.params.id)
      console.log(this.leilao)
      this.nome = this.leilao.nome
      this.valor_entrada = this.leilao.valor_inicial
      this.valor_aumento = this.leilao.valor_aumento
      this.end_date = this.leilao.data_final
      this.id_usuario = this.leilao.id_usuario
    }
    this.id_usuario = LoginStorage.value.id
    document.querySelectorAll('input').forEach(i => {
      i.oninput = () => {
        if(this.leilao) this.leilao[i.id] = i.value
        this[i.id] = i.value
        console.log(this)
      }
    })
  }
  render() {
    return (
      <div class="form-conteiner">
        <form onSubmit={(e) => this.create(e)}>
          <div class="form-group">
            <label htmlFor="name">Nome Do Leilão</label>
            <input type="name" class="form-control" value={this.nome} id="nome" aria-describedby="emailHelp" placeholder="Nome do Seu Leilão" />
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label htmlFor="nomeItem">Nome do Item</label>
                <input type="text" value={this.item_name} class="form-control" id="item_name" placeholder="Nome do seu item" />
              </div>
              <div class="col-md-4">
                <label htmlFor="imgItem">Imagem do item</label>
                <input type="file" value={this.item_image} class="form-control" id="item_image" />
              </div>
              <div class="col-md-2">
                <label htmlFor="valorItem">Valor do item</label>
                <input type="number" value={this.item_value} id="item_value" class="form-control" placeholder="500" />
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <label htmlFor="valorEntrada">Valor minimo para lance</label>
              <input type="number" value={this.valor_entrada} id="valor_entrada" class="form-control" />
            </div>
            <div class="form-group">
              <label htmlFor="valorAumento">Valor minimo de aumento</label>
              <input type="number" value={this.valor_aumento} id="valor_aumento" class="form-control" />
            </div>
            <div class="form-group">
              <label htmlFor="dataMaxima">Data Maxima da existencia do leilão</label>
              <input type="datetime-local" value={this.end_date} class="form-control" id="end_date" placeholder="21/09/2020" />
            </div>
          </div>
          <button class="btn btn-primary">Enviar</button>
        </form>
      </div>
    );
  }
  create(e) {
    e.preventDefault()
    const pack: ANewLeilao = {
      nome: this.nome,
      valor_entrada: this.valor_entrada,
      valor_aumento: this.valor_aumento,
      end_date: this.end_date,
      id_usuario: 1,
      item_name: this.item_name,
      item_value: this.item_value,
      item_image: this.item_image,
    }
    console.log(pack)
    if(!this.match.params.id){
    leilaoService.createWithItem(pack)
    }else{
      leilaoService.edit(+this.match.params.id,this.leilao)
    }

  }

}
